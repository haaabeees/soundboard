# Welcome #
This soundboard is an application that is currently under development. The main purpose of the soundboard is to help streamers and friends to add sounds to a voice chat or a stream with ease.

# Start Using #

1. Browse a folder or a file location

2. Add/Load so it is added to the list
 
3. Play

Under the devices tab you can change which device should play the sound.

If you want to play sounds through your microphone I recommend using the Virtual Audio Cable driver. And add the device it creates as the secondary device on the devices tab.

The general volume is ignored for a file when you change that file's volume.

# Start Helping #
Install: Visual Studio 2017 (community works fine)
Make sure you can create and edit WPF with it.

# Libraries Used #
NAudio

Newtonsoft JSon