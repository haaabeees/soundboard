﻿using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;

namespace EarBleedWPF
{
    internal static class SoundboardController
    {
        public static ObservableCollection<Sound> soundList;
        private static WaveOut waveOutDevice;
        private static AudioFileReader audioFileReader;
        public static int DeviceNumber { get; set; }
        public static int SecondaryDeviceNumber { get; set; }
        public static float DeviceNumber1Volume { get; set; }
        public static float DeviceNumber2Volume { get; set; }
        public static ConfigurationController cc;
        public static bool isSoundPlaying;
        public static int lastPlayedSound;
        public static bool playMultipleSounds = false;

        static SoundboardController()
        {
            soundList = new ObservableCollection<Sound>();
            waveOutDevice = new WaveOut();
            cc = new ConfigurationController();
            if (null != cc.ImportMusicLocations())
                soundList = cc.ImportMusicLocations();

            lastPlayedSound = 0;
            isSoundPlaying = false;
        }

        //- improve code here
        private static void SetSingleWaveOutConfig(float volume, bool isUsingSecondaryDevice)
        {
            if (DeviceNumber != 100)
                waveOutDevice.DeviceNumber = DeviceNumber;
            if (volume > 0 && volume <= 100)
                waveOutDevice.Volume = volume / 100;
            else
                waveOutDevice.Volume = DeviceNumber1Volume / 100;

            if (isUsingSecondaryDevice)
            {
                waveOutDevice.DeviceNumber = SecondaryDeviceNumber;
                if (volume > 0 && volume <= 100)
                    waveOutDevice.Volume = volume / 100;
                else
                    waveOutDevice.Volume = DeviceNumber2Volume / 100;
            }
        }

        //Main Play Method - improve code here
        public static bool PlaySoundWithIndex(int index, bool isUsingSecondaryDevice)
        {
            try
            {
                if (playMultipleSounds)
                    isSoundPlaying = false;
                if (!isSoundPlaying)
                {
                
                    SetSingleWaveOutConfig(soundList[index].Volume, false);
                    audioFileReader = new AudioFileReader(soundList[index].Path);

                    //Prepares to play a sound which could have its start and end time changed
                    var trimmed = new OffsetSampleProvider(audioFileReader);
                    trimmed.SkipOver = TimeSpan.FromMilliseconds(soundList[index].StartTime);
                    trimmed.Take = TimeSpan.FromMilliseconds(audioFileReader.TotalTime.TotalMilliseconds - soundList[index].EndTime);
                    waveOutDevice.Init(trimmed);
                    waveOutDevice.Play();
                    isSoundPlaying = true;
                    waveOutDevice.PlaybackStopped += OnPlaybackStopped;

                    if (isUsingSecondaryDevice && DeviceNumber != SecondaryDeviceNumber)
                    {
                        audioFileReader = new AudioFileReader(soundList[index].Path);
                        trimmed = new OffsetSampleProvider(audioFileReader);
                        trimmed.SkipOver = TimeSpan.FromMilliseconds(soundList[index].StartTime);
                        trimmed.Take = TimeSpan.FromMilliseconds(audioFileReader.TotalTime.TotalMilliseconds - soundList[index].EndTime);
                        SetSingleWaveOutConfig(soundList[index].Volume, isUsingSecondaryDevice);
                        waveOutDevice.Init(trimmed);
                        waveOutDevice.Play();
                        isSoundPlaying = true;
                        waveOutDevice.PlaybackStopped += OnPlaybackStopped;
                    }

                    SetLastPlayedSound((soundList[index]));
                }
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Play Sound with path - Invalid file/path! " + ex.StackTrace);
                return false;
            }
        }

        public static void SetLastPlayedSound(Sound s)
        {
            lastPlayedSound = soundList.IndexOf(s);
        }

        public static void SetLastPlayedSound(string path, float volume)
        {
            for (int i = 0; i < soundList.Count; i++)
            {
                if (soundList[i].Path == path && soundList[i].Volume == volume)
                {
                    lastPlayedSound = i;
                    return;
                }
            }
            lastPlayedSound = -1;
        }

        private static void OnPlaybackStopped(object sender, StoppedEventArgs e)
        {
            isSoundPlaying = false;
        }

        public static ObservableCollection<Sound> GetSoundList()
        {
            return soundList;
        }

        public static Sound GetSound(int index)
        {
            return soundList[index];
        }

        //Stop active sound
        public static void StopPlaying()
        {
            waveOutDevice.Stop();
            isSoundPlaying = false;
        }

        //Load all sounds from a folder
        public static bool LoadSounds(string directory)
        {
            try
            {
                DirectoryInfo soundsDir = new DirectoryInfo(directory);
                foreach (FileInfo soundFile in soundsDir.GetFiles("*.mp3"))
                {
                    soundList.Add(new Sound(soundFile.FullName));
                }
                cc.ExportMusicLocations(soundList);
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("LoadSounds - Invalid folder  " + ex.StackTrace);
                return false;
            }
        }

        //Add a single sound given a path
        public static bool AddSound(string path)
        {
            if (path.Contains(".mp3") || path.Contains(".wav") || path.Contains(".flac"))
            {
                soundList.Add(new Sound(path));
                cc.ExportMusicLocations(soundList);
                return true;
            }
            Debug.WriteLine("Add sound - Invalid file");
            return false;
        }

        public static void SaveChanges()
        {
            cc.ExportMusicLocations(soundList);
        }

        public static void RemoveSound(Sound s)
        {
            soundList.Remove(s);
        }

        public static void ImportSoundConfiguration(string path)
        {
            ObservableCollection<Sound> aux = cc.ImportMusicLocations(path);

            foreach (Sound s in aux)
            {
                soundList.Add(s);
            }
        }

        //Pause playback
    }
}