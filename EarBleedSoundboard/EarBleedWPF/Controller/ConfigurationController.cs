﻿using EarBleedWPF.Model;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;

namespace EarBleedWPF
{
    internal class ConfigurationController
    {
        //Exports sound locations and configurations to the default folder
        public void ExportMusicLocations(ObservableCollection<Sound> soundList)
        {
            try
            {
                DataContainer dataC = new DataContainer();
                dataC.data = soundList;
                string outputJSON = Newtonsoft.Json.JsonConvert.SerializeObject(dataC, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(AppDomain.CurrentDomain.BaseDirectory + @"SoundsConfiguration.json", outputJSON + Environment.NewLine);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
        }

        public void ExportMusicLocations(ObservableCollection<Sound> soundList, string customPath)
        {
            try
            {
                DataContainer dataC = new DataContainer();
                dataC.data = soundList;
                string outputJSON = Newtonsoft.Json.JsonConvert.SerializeObject(dataC, Newtonsoft.Json.Formatting.Indented);
                File.WriteAllText(customPath + "" + @"SoundsConfiguration.json", outputJSON + Environment.NewLine);
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
            }
        }

        //Imports sound locations and configurations from the default folder
        public ObservableCollection<Sound> ImportMusicLocations()
        {
            try
            {
                if (File.Exists(AppDomain.CurrentDomain.BaseDirectory + @"SoundsConfiguration.json"))
                {
                    String JSONtxt = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"SoundsConfiguration.json");
                    var dataContainer = Newtonsoft.Json.JsonConvert.DeserializeObject<DataContainer>(JSONtxt);

                    return dataContainer.data;
                }
                return null;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return null;
            }
        }

        public ObservableCollection<Sound> ImportMusicLocations(string customPath)
        {
            try
            {
                if (File.Exists(customPath))
                {
                    String JSONtxt = File.ReadAllText(AppDomain.CurrentDomain.BaseDirectory + @"SoundsConfiguration.json");
                    var dataContainer = Newtonsoft.Json.JsonConvert.DeserializeObject<DataContainer>(JSONtxt);

                    return dataContainer.data;
                }
                return null;
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.StackTrace);
                return null;
            }
        }
    }
}