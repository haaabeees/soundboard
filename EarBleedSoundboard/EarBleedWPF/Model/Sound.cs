﻿namespace EarBleedWPF
{
    internal class Sound
    {
        public string Path { get; set; }
        public string Name { get; set; }
        public float Volume { get; set; }
        public int StartTime { get; set; }
        public int EndTime { get; set; }
        public string Shortcut { get; set; }

        public Sound(string path)
        {
            Path = path;
            Volume = 0;
            StartTime = 0;
            Name = System.IO.Path.GetFileName(Path);
        }

        public override string ToString()
        {
            return System.IO.Path.GetFileName(Path);
        }
    }
}