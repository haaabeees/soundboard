﻿using System.Collections.ObjectModel;

namespace EarBleedWPF.Model
{
    internal class DataContainer
    {
        public ObservableCollection<Sound> data = new ObservableCollection<Sound>();
    }
}