﻿using NAudio.Wave;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;

namespace EarBleedWPF
{
    /// <summary>
    /// Interaction logic for SoundEditWindow.xaml
    /// </summary>
    public partial class SoundEditWindow : Window
    {
        public SoundEditWindow()
        {
            InitializeComponent();
        }

        private void BrowseButton_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new OpenFileDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (!string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    pathTextBox.Text = fbd.FileName;
                }
            }
        }

        private void PlayButtonClick(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button button = (System.Windows.Controls.Button)sender;

            if (!SoundboardController.PlaySoundWithIndex(SoundboardController.soundList.IndexOf(((Sound)button.DataContext)), false))
                System.Windows.Forms.MessageBox.Show("Invalid file/path!");
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            SoundboardController.StopPlaying();
        }

        private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!char.IsDigit(e.Text, e.Text.Length - 1))
                e.Handled = true;
        }

        private void GetFileInfo()
        {
            try
            {
                AudioFileReader afr = new AudioFileReader(pathTextBox.Text);
                totalTimeTextBox.Text = "" + afr.TotalTime.TotalMilliseconds;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show("Unable to get info");
                Debug.WriteLine(ex.StackTrace);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            GetFileInfo();
        }

        private void PathTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            startTimeTextBox.Text = "0";
            endTimeTextBox.Text = "0";
            GetFileInfo();
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            SoundboardController.cc.ExportMusicLocations(SoundboardController.soundList);
        }
    }
}