﻿using NAudio.Wave;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;

namespace EarBleedWPF
{
    /// <summary>
    /// SoundBleed main window
    /// </summary>
    public partial class MainWindow : Window
    {
        private InterceptKeys kh;

        public MainWindow()
        {
            kh = new InterceptKeys(true);
            kh.KeyDown += Kh_KeyDown;
            InitializeComponent();
            SetComboBoxItems();
            listboxFolder1.ItemsSource = SoundboardController.soundList;
        }

        //Soundlist browse file
        private void BrowseFileButtonSoundList_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new OpenFileDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (!string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    soundListFileTextbox.Text = fbd.FileName;
                }
            }
        }

        //Soundlist browse folder
        private void BrowseFolderButtonSoundList_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (!string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    soundListFolderTextbox.Text = fbd.SelectedPath;
                }
            }
        }

        //Soundlist add  file
        private void AddButtonSoundList_Click(object sender, RoutedEventArgs e)
        {
            if (!SoundboardController.AddSound(soundListFileTextbox.Text))
                System.Windows.Forms.MessageBox.Show("Invalid file!");
        }

        //Soundlist load folder
        private void LoadFolderButtonSoundList_Click(object sender, RoutedEventArgs e)
        {
            if (!SoundboardController.LoadSounds(soundListFolderTextbox.Text))
                System.Windows.Forms.MessageBox.Show("Invalid folder/folder path!");
        }

        //SoundBoard browse file
        private void BrowseFileButtonSoundBoard_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new OpenFileDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (!string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    soundBoardFileTextbox.Text = fbd.FileName;
                }
            }
        }

        //SoundBoard browse folder
        private void BrowseFolderButtonSoundBoard_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (!string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    soundBoardFolderSelectTextbox.Text = fbd.SelectedPath;
                }
            }
        }

        //Soundboard add folder
        private void AddButtonSoundBoard_Click(object sender, RoutedEventArgs e)
        {
            if (!SoundboardController.AddSound(soundBoardFileTextbox.Text))
                System.Windows.Forms.MessageBox.Show("Invalid file!");
        }

        //SoundBoard load folder
        private void LoadFolderSoundBoard_Click(object sender, RoutedEventArgs e)
        {
            if (!SoundboardController.LoadSounds(soundBoardFolderSelectTextbox.Text))
                System.Windows.Forms.MessageBox.Show("Invalid folder/folder path!");
        }

        //Menu item load file
        private void LoadFileMenuItem_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new OpenFileDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (!string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    if (!SoundboardController.AddSound(fbd.FileName))
                        System.Windows.Forms.MessageBox.Show("Invalid file!");
                }
            }
        }

        //Menu item load folder
        private void LoadFolderMenuItem_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (!string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    if (!SoundboardController.LoadSounds(fbd.SelectedPath))
                        System.Windows.Forms.MessageBox.Show("Invalid folder/folder path!");
                }
            }
        }

        //Menu item save changes
        private void SaveChangesMenuItem_Click(object sender, RoutedEventArgs e)
        {
            SoundboardController.cc.ExportMusicLocations(SoundboardController.soundList);
        }

        private void ExitMenuItem_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void PlayButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                System.Windows.Controls.Button button = (System.Windows.Controls.Button)sender;
                if (listboxFolder1.HasItems)
                {
                    if (!SoundboardController.PlaySoundWithIndex(SoundboardController.soundList.IndexOf(((Sound)button.DataContext)), useSecondaryCheckBox.IsChecked.Value))
                        System.Windows.Forms.MessageBox.Show("Invalid file/path!");
                }
            }
            catch (Exception ex)
            {
                if (SoundboardController.soundList.Count >= 1 && SoundboardController.lastPlayedSound != -1)
                    SoundboardController.PlaySoundWithIndex(SoundboardController.lastPlayedSound, useSecondaryCheckBox.IsChecked.Value);
                else
                {
                    System.Windows.Forms.MessageBox.Show("No sound to play!");
                    Debug.WriteLine(ex.StackTrace);
                }
            }
        }

        private void PlayFromListButton_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button button = (System.Windows.Controls.Button)sender;

            if (!SoundboardController.PlaySoundWithIndex(SoundboardController.soundList.IndexOf(((Sound)button.DataContext)), useSecondaryCheckBox.IsChecked.Value))
                System.Windows.Forms.MessageBox.Show("Invalid file/path!");
        }

        private void StopButton_Click(object sender, RoutedEventArgs e)
        {
            SoundboardController.StopPlaying();
        }

        //Global Key detection
        private void Kh_KeyDown(Keys key, bool Shift, bool Ctrl, bool Alt)
        {
            Keys k2, k3, k4, k5;
            Enum.TryParse(stopKeyTextbox.Text, out k2);
            Enum.TryParse(volUpTextBox.Text, out k4);
            Enum.TryParse(volDownTextBox.Text, out k5);
            if (key == k2)
            {
                SoundboardController.StopPlaying();
            }
            if (key == k4)
            {
                device1VolumeSlider.Value += 0.5;
            }
            if (key == k5)
            {
                device1VolumeSlider.Value -= 0.5;
            }

            //For every sound in the sound list check for shortcuts
            foreach (Sound s in SoundboardController.soundList)
            {
                Enum.TryParse(s.Shortcut, out k3);
                if (key == k3)
                {
                    if (!SoundboardController.PlaySoundWithIndex(SoundboardController.lastPlayedSound, useSecondaryCheckBox.IsChecked.Value))
                        System.Windows.Forms.MessageBox.Show("Invalid file/Path!");
                }
            }
        }

        private void VolumeDevice1Slider_ValueChanged(Object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SoundboardController.DeviceNumber1Volume = (float)device1VolumeSlider.Value;
        }

        private void VolumeDevice2Slider_ValueChanged(Object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SoundboardController.DeviceNumber2Volume = (float)device2VolumeSlider.Value;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SoundboardController.DeviceNumber = outputDevicesComboBox.SelectedIndex;
        }

        //Gets all waveout devices and set the current selected item to the first device;
        private void SetComboBoxItems()
        {
            int waveOutDevices = WaveOut.DeviceCount;
            for (int waveOutDevice = 0; waveOutDevice < waveOutDevices; waveOutDevice++)
            {
                WaveOutCapabilities deviceInfo = WaveOut.GetCapabilities(waveOutDevice);
                Console.WriteLine("Device {0}: {1}, {2} channels", waveOutDevice, deviceInfo.ProductName, deviceInfo.Channels);
                outputDevicesComboBox.Items.Add(deviceInfo.ProductName);
                secondaryOutputDevicesComboBox.Items.Add(deviceInfo.ProductName);
            }

            outputDevicesComboBox.SelectedItem = outputDevicesComboBox.Items.GetItemAt(0);
            secondaryOutputDevicesComboBox.SelectedItem = outputDevicesComboBox.Items.GetItemAt(0);
        }

        private void SecondaryDeviceComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            SoundboardController.SecondaryDeviceNumber = secondaryOutputDevicesComboBox.SelectedIndex;
        }

        private void TextBlock_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (listboxFolder1.HasItems && (e.LeftButton == MouseButtonState.Pressed && e.ClickCount == 2))
            {
                System.Windows.Controls.TextBlock tb = (System.Windows.Controls.TextBlock)sender;
                SoundboardController.PlaySoundWithIndex(SoundboardController.soundList.IndexOf(((Sound)tb.DataContext)), useSecondaryCheckBox.IsChecked.Value);
            }
            else
            {
                Debug.WriteLine("Play button - no sound to be played.");
            }
        }

        private void EditFromList_Click(object sender, RoutedEventArgs e)
        {
            SoundEditWindow soundEditWindow = new SoundEditWindow { Owner = this };
            System.Windows.Controls.Button tb = (System.Windows.Controls.Button)sender;
            soundEditWindow.DataContext = ((Sound)tb.DataContext);

            soundEditWindow.Show();
        }

        private void SoundButton_EditMenuClick(object sender, RoutedEventArgs e)
        {
            SoundEditWindow soundEditWindow = new SoundEditWindow { Owner = this };
            System.Windows.Controls.MenuItem tb = (System.Windows.Controls.MenuItem)sender;
            soundEditWindow.DataContext = ((Sound)tb.DataContext);

            soundEditWindow.Show();
        }

        private void SoundButton_DeleteMenuClick(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.MenuItem tb = (System.Windows.Controls.MenuItem)sender;

            SoundboardController.RemoveSound(((Sound)tb.DataContext));

            SoundboardController.SaveChanges();
        }

        private void SoundButton_DeleteSoundListClick(object sender, RoutedEventArgs e)
        {
            System.Windows.Controls.Button tb = (System.Windows.Controls.Button)sender;

            SoundboardController.RemoveSound(((Sound)tb.DataContext));

            SoundboardController.SaveChanges();
        }

        private void ExportSoundConfigurationMenuItem_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (!string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    SoundboardController.cc.ExportMusicLocations(SoundboardController.soundList, fbd.SelectedPath);
                }
            }
        }

        private void ImportSoundConfigurationMenuItem_Click(object sender, RoutedEventArgs e)
        {
            using (var fbd = new OpenFileDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (!string.IsNullOrWhiteSpace(fbd.FileName))
                {
                    SoundboardController.ImportSoundConfiguration(fbd.FileName);
                }
            }
        }

        private void PlayMultipleSoundCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            SoundboardController.playMultipleSounds = true;
        }
        private void PlayMultipleSoundCheckBox_Unchecked(object sender, RoutedEventArgs e)
        {
            SoundboardController.playMultipleSounds = false;
        }
    }
}